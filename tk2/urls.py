"""tk2 URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.urls import path, include
from django.contrib import admin
from rest_framework.urlpatterns import format_suffix_patterns
from simulasi import views as simulasi_view
from rating import views as rating_view

urlpatterns = [
    path('admin/', admin.site.urls),
    path('', include('homepage.urls')),
    path('rating/',include('rating.urls')),
    path('simulasi/', include('simulasi.urls')),
    path('simulasi/positif/', simulasi_view.PendudukList.as_view()),
    path('persentasecovid/', include('statistik.urls')),
    path('feedback/', include('feedback.urls')),
    path('',include("django.contrib.auth.urls")),
    path('signup/',include('register.urls')),
    path('rating/akun/',rating_view.AkunList.as_view()),
]

urlpatterns = format_suffix_patterns(urlpatterns)