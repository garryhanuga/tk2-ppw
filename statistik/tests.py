from django.test import TestCase, Client
from simulasi.models import JumlahPendudukAwal
from simulasi.models import Penduduk
from statistik.models import PersentaseStatus
from statistik.models import PersentaseJenisKelamin
from statistik.models import PersentaseUmur
from django.urls import resolve
from . import views
from django.contrib import auth
from django.contrib.auth.models import User
from django.contrib.auth import login, get_user

class StatisticTest(TestCase):

    def test_url_statistik(self):
        user = get_user(self.client)
        response = Client().get("/persentasecovid/")
        self.assertEqual(response.status_code, 302)
        
        self.user = User.objects.create_user(username='testuser', password='12345')
        login = self.client.login(username='testuser', password='12345')
        response = self.client.get("/persentasecovid/")
        self.assertEqual(response.status_code, 200)

    def test_template_statistik(self):
        user = get_user(self.client)
        self.user = User.objects.create_user(username='testuser', password='12345')
        login = self.client.login(username='testuser', password='12345')
        response = self.client.get('/persentasecovid/')
        self.assertTemplateUsed(response, 'stats.html')

    def test_halaman_statistik_if_data(self):
        user = get_user(self.client)
        self.user = User.objects.create_user(username='testuser', password='12345')
        login = self.client.login(username='testuser', password='12345')
        JumlahPendudukAwal.objects.create(jumlah=10)
        response = self.client.get('/persentasecovid/')
        fields = response.content.decode('utf8')
        self.assertIn("Filter", fields)
        self.assertIn("Coba Lagi", fields)

    def test_halaman_statistik_if_no_data(self):
        user = get_user(self.client)
        self.user = User.objects.create_user(username='testuser', password='12345')
        login = self.client.login(username='testuser', password='12345')
        response = self.client.get('/persentasecovid/')
        fields = response.content.decode('utf8')
        self.assertIn("Data belum dimasukkan", fields)
        self.assertIn("Mulai Simulasi", fields)

    def test_func_statCovid_exists(self):
        user = get_user(self.client)
        self.user = User.objects.create_user(username='testuser', password='12345')
        login = self.client.login(username='testuser', password='12345')
        find = resolve('/persentasecovid/')
        self.assertEqual(find.func, views.statCovid)

    def test_func_cobaLagi_exists(self):
        user = get_user(self.client)
        self.user = User.objects.create_user(username='testuser', password='12345')
        login = self.client.login(username='testuser', password='12345')
        find = resolve('/persentasecovid/reset/')
        self.assertEqual(find.func, views.cobaLagi)

    def test_func_reset_redirect(self):
        user = get_user(self.client)
        response = Client().get("/persentasecovid/")
        self.assertEqual(response.status_code, 302)
        
        self.user = User.objects.create_user(username='testuser', password='12345')
        login = self.client.login(username='testuser', password='12345')
        response = self.client.get("/persentasecovid/reset/")
        self.assertEqual(response.status_code, 302)

    def test_func_reset_jumlah_awal(self):
        user = get_user(self.client)
        self.user = User.objects.create_user(username='testuser', password='12345')
        login = self.client.login(username='testuser', password='12345')
        JumlahPendudukAwal.objects.create(jumlah=10)
        response = self.client.get('/persentasecovid/reset/')
        jumlah_awal = JumlahPendudukAwal.objects.all().count()
        self.assertEquals(jumlah_awal, 0)
    
    def test_func_reset_penduduk_baru(self):
        user = get_user(self.client)
        self.user = User.objects.create_user(username='testuser', password='12345')
        login = self.client.login(username='testuser', password='12345')
        Penduduk.objects.create(nama_penduduk="ABC",usia="20", alamat="ABC", jenis_kelamin="Laki-Laki", status_covid="Positif")
        response = self.client.get('/persentasecovid/reset/')
        jumlah_penduduk = Penduduk.objects.all().count()
        self.assertEquals(jumlah_penduduk, 0)

    def test_func_statCovid_ajax_exists(self):
        user = get_user(self.client)
        self.user = User.objects.create_user(username='testuser', password='12345')
        login = self.client.login(username='testuser', password='12345')
        find = resolve('/persentasecovid/results/')
        self.assertEqual(find.func, views.statCovid_ajax)

    def test_model_status(self):
    	PersentaseStatus.objects.create(positif=40, negatif=60)
    	jumlah = PersentaseStatus.objects.all().count()
    	self.assertEquals(jumlah, 1)

    def test_model_jenis_kelamin(self):
    	PersentaseJenisKelamin.objects.create(pria=50, perempuan=50)
    	jumlah = PersentaseJenisKelamin.objects.all().count()
    	self.assertEquals(jumlah, 1)

    def test_model_umur(self):
    	PersentaseUmur.objects.create(muda=25, tengah=25, dewasa=25, tua=25)
    	jumlah = PersentaseUmur.objects.all().count()
    	self.assertEquals(jumlah, 1)
