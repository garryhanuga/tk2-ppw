$(document).ready(function() {    
    $(".filter").change(function() {
        var filter = $(".filter").val();
        $.ajax({
            url : '/persentasecovid/results',
            success : function(response) {
                var persen = response.data;
                $("#statistik").empty();

                var status = "<div><h3>Persentase Berstatus Positif COVID-19</h3><br>";
                status += "<div class='circle' style='background: #6FCF97'>";
                status += "<p>" + persen.positif + "%</p></div></div>";
                status += "<div><h2>Persentase Berstatus Negatif COVID-19</h2><br>";
                status += "<div class='circle' style='background: #f44336'>";
                status += "<p>" + persen.negatif + "%</p></div></div>";

                var gender = "<div><h2>Persentase Kasus Positif yang Laki-laki</h2><br>";
                gender += "<div class='circle' style='background: #4169E1'>";
                gender += "<p>" + persen.pria + "%</p></div></div>";
                gender += "<div><h2>Persentase Kasus Positif yang Perempuan</h2><br>";
                gender += "<div class='circle' style='background: #F08080'>";
                gender += "<p>" + persen.perempuan + "%</p></div></div>";

                var umur = "<div><h2>Persentase Kasus Positif yang Berusia 0-20</h2><br>";
                umur += "<div class='circle' style='background: #D8BFD8'>";
                umur += "<p>" + persen.muda + "%</p></div></div>";
                umur += "<div><h2>Persentase Kasus Positif yang Berusia 21-44</h2><br>";
                umur += "<div class='circle' style='background: #DDA0DD'>";
                umur += "<p>" + persen.dewasa + "%</p></div></div>";
                umur += "<div><h2>Persentase Kasus Positif yang Berusia 45-65</h2><br>";
                umur += "<div class='circle' style='background: #BA55D3'>";
                umur += "<p>" + persen.tengah + "%</p></div></div>";
                umur += "<div><h2>Persentase Kasus Positif yang Berusia 66+</h2><br>";
                umur += "<div class='circle' style='background: #8B008B'>";
                umur += "<p>" + persen.tua + "%</p></div></div>";

                if (filter == "semua") {
                    $("#statistik").append(status);
                    $("#statistik").append(gender);
                    $("#statistik").append(umur);
                }
                else if (filter == "status") {
                    $("#statistik").append(status);
                }
                else if (filter == "gender") {
                    $("#statistik").append(gender);
                }
                else if (filter == "umur") {
                    $("#statistik").append(umur);
                }
            }
        });
    });
});

$("#simulasi").hover(function() {
    $(this).css("background-color", "#55a170");
    }, function() {
    $(this).css("background-color", "#6FCF97");
});

$("#reset").hover(function() {
    $(this).css("background-color", "#55a170");
    }, function() {
    $(this).css("background-color", "#6FCF97");
});
