from django.contrib import admin
from .models import PersentaseStatus, PersentaseJenisKelamin, PersentaseUmur

admin.site.register(PersentaseStatus)
admin.site.register(PersentaseJenisKelamin)
admin.site.register(PersentaseUmur)