from django.test import TestCase, Client
from django.urls import reverse, resolve
from django.contrib.auth import get_user
from django.contrib.auth.models import User
from .views import listfeedback, tambah, saveform
from .models import feedback

class feedbackTest(TestCase):

    
    def test_feedback_urls_is_exist(self):
        user = get_user(self.client)
        response = Client().get("/feedback/")
        self.assertEqual(response.status_code,302)
        
        self.user = User.objects.create_user(username='testuser', password='12345')
        login = self.client.login(username='testuser', password='12345')
        response = self.client.get("/feedback/")
        self.assertEqual(response.status_code,200)

    
    def test_feedback_templates_is_exist(self):
        user = get_user(self.client)
        self.user = User.objects.create_user(username='testuser', password='12345')
        login = self.client.login(username='testuser', password='12345')
        response = self.client.get("/feedback/")
        self.assertTemplateUsed(response, 'feedback.html')
  
    def test_feedback_using_func(self):
        found = resolve('/feedback/')
        self.assertEqual(found.func, listfeedback)

    def test_tambah_urls_is_exist(self):
        user = get_user(self.client)
        response = Client().get('/feedback/tambah/')
        self.assertEqual(response.status_code,302)

        self.user = User.objects.create_user(username='testuser', password='12345')
        login = self.client.login(username='testuser', password='12345')
        response = self.client.get('/feedback/tambah/')
        self.assertEqual(response.status_code,200)

    def test_tambah_templates_is_exist(self):
        self.user = User.objects.create_user(username='testuser', password='12345')
        login = self.client.login(username='testuser', password='12345')
        response = self.client.get('/feedback/tambah/')
        self.assertTemplateUsed(response, 'tambah.html')
    
    def test_tambah_using_func(self):
        found = resolve('/feedback/tambah/')
        self.assertEqual(found.func, tambah)

    def test_saveform_url_is_exist(self):
        self.user = User.objects.create_user(username='testuser', password='12345')
        login = self.client.login(username='testuser', password='12345')
        response = self.client.post('/feedback/saveform/')
        self.assertEqual(response.status_code, 302)

    def test_can_save_POST_request(self):
        self.user = User.objects.create_user(username='testuser', password='12345')
        login = self.client.login(username='testuser', password='12345')
        response = self.client.post('/feedback/saveform/', data={ 
            'pesan' : 'b','img':'/static/1.png'})
        count_all = feedback.objects.all().count()
        self.assertEqual(count_all, 1)

        self.assertEqual(response.status_code,302)

    def test_model_can_create_new_activity(self):
        new_feeds = feedback.objects.create(nama="a",pesan="b",img="1",num="2")
        new_feeds.save()
        count_all = feedback.objects.all().count()
        self.assertEqual(count_all, 1)

    def feedback_urls_is_exist(self):
        response = Client().get('/feedback/')
        self.assertEqual(response.status_code,200)

    def feedback_templates_is_exist(self):
        response = Client().get('/feedback/')
        self.assertTemplateUsed(response, 'feedback.html')
    
    def feedback_using_func(self):
        found = resolve('/feedback/')
        self.assertEqual(found.func, listfeedback)

    def tambah_urls_is_exist(self):
        response = Client().get('/feedback/tambah/')
        self.assertEqual(response.status_code, 200)

    def tambah_templates_is_exist(self):
        response = Client().get('/feedback/tambah')
        self.assertTemplateUsed(response, 'tambah.html')
    
    def tambah_using_func(self):
        found = resolve('/feedback/tambah')
        self.assertEqual(found.func, tambah)

